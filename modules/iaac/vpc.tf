resource "aws_vpc" "tg-vpc" {
    cidr_block = var.vpc_cidr_block
    instance_tenancy = "default"

    tags = {
      "Name" = "technogeeks-${var.indentfier}-vpc",
      "Created By" = var.created_by
    }
  
}

resource "aws_subnet" "tg-public-subnet" {
  vpc_id = aws_vpc.tg-vpc.id
  cidr_block = var.public_subnet_cidr
  availability_zone = var.public_subnet_az
  
  tags = {
      "Name" = "technogeeks-${var.indentfier}-public-subnet",
      "Created By" = var.created_by
    }

  
}

resource "aws_subnet" "tg-private-subnet" {
  vpc_id = aws_vpc.tg-vpc.id
  cidr_block = var.private_subnet_cidr
  availability_zone = var.private_subnet_az

  tags = {
      "Name" = "technogeeks-${var.indentfier}-private-subnet",
      "Created By" = var.created_by
    }
  
}

resource "aws_internet_gateway" "tg-igw" {
  vpc_id = aws_vpc.tg-vpc.id

  tags = {
      "Name" = "technogeeks-${var.indentfier}-igw",
      "Created By" = var.created_by
    }
  
}

resource "aws_route_table" "tg-public-rt" {
  vpc_id = aws_vpc.tg-vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.tg-igw.id
  }
  
  tags = {
      "Name" = "technogeeks-${var.indentfier}-public-rt",
      "Created By" = var.created_by
    }
}

resource "aws_route_table" "tg-private-rt" {
  vpc_id = aws_vpc.tg-vpc.id

  tags = {
      "Name" = "technogeeks-${var.indentfier}-private-rt",
      "Created By" = var.created_by
    }
  
}

resource "aws_route_table_association" "tg-public-association" {
  subnet_id = aws_subnet.tg-public-subnet.id
  route_table_id = aws_route_table.tg-public-rt.id
}

resource "aws_route_table_association" "tg-private-association" {
  subnet_id = aws_subnet.tg-private-subnet.id
  route_table_id = aws_route_table.tg-private-rt.id
  
}
