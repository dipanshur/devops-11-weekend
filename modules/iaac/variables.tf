variable "indentfier" {
  description = "value to identify the resources"
}

variable "vpc_cidr_block" {
    description = "value of vpc cidr block"
  
}

variable "created_by" {
  description = "value for created by, defaults to VPC module"
}

variable "public_subnet_cidr" {
  description = "value of public subnet cidr range"
}

variable "private_subnet_cidr" {
    description = "value of private subnets cidr ranges"
  
}

variable "public_subnet_az" {
  description = "value of availability zones for public subnets"
}

variable "private_subnet_az" {
    description = "value of availability zones for private subnets"
  
}