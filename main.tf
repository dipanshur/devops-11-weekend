module "vpc-stack-devops-11" {
    source = "./modules/iaac"

    indentfier = "devops-11"
    vpc_cidr_block = "192.168.0.0/16"
    created_by = "John"
    public_subnet_az = "ap-south-1b"
    public_subnet_cidr = "192.168.0.0/24"
    private_subnet_az = "ap-south-1c"
    private_subnet_cidr = "192.168.1.0/24"
  
}

/*
module "vpc-stack-devops-12" {
    source = "./modules/iaac"

    indentfier = "devops-12"
    vpc_cidr_block = "192.168.0.0/16"
    created_by = "Dipanshu"
    public_subnet_az = "ap-south-1a"
    public_subnet_cidr = "192.168.0.0/24"
    private_subnet_az = "ap-south-1b"
    private_subnet_cidr = "192.168.1.0/24"
  
}

module "vpc-stack-devops-13" {
    source = "./modules/iaac"

    indentfier = "devops-13"
    vpc_cidr_block = "192.168.0.0/16"
    created_by = "Dipanshu"
    public_subnet_az = "ap-south-1a"
    public_subnet_cidr = "192.168.0.0/24"
    private_subnet_az = "ap-south-1b"
    private_subnet_cidr = "192.168.1.0/24"
  
}

module "vpc-stack-devops-14" {
    source = "./modules/iaac"

    indentfier = "devops-14"
    vpc_cidr_block = "192.168.0.0/16"
    created_by = "Dipanshu"
    public_subnet_az = "ap-south-1a"
    public_subnet_cidr = "192.168.0.0/24"
    private_subnet_az = "ap-south-1b"
    private_subnet_cidr = "192.168.1.0/24"
  
}


module "vpc-stack-devops-15" {
    source = "./modules/iaac"

    indentfier = "devops-15"
    vpc_cidr_block = "172.31.0.0/16"
    created_by = "Dipanshu"
    public_subnet_az = "ap-south-1a"
    public_subnet_cidr = "172.31.0.0/24"
    private_subnet_az = "ap-south-1b"
    private_subnet_cidr = "172.31.1.0/24"
  
}*/