terraform {
  backend "s3" {
    bucket = "tg-11am-state-bucket-ap-south-1"
    key    = "tg-11am-state-bucket-ap-south-1/tfstate/terraform.tfstate"
    region = "ap-south-1"
    
  }
}